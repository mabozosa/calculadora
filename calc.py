#!/usr/bin/python3

__author__ = 'MABB'

''' Calculadora simple con funciones sumar y restar '''


def sumar(sumando1, sumando2):

    sumando1 = float(sumando1)
    sumando2 = float(sumando2)
    suma = sumando1 + sumando2

    print(suma)


def restar(minuendo, sustraendo):

    minuendo = float(minuendo)
    sustraendo = float(sustraendo)
    diferencia = minuendo - sustraendo

    print(diferencia)


if __name__ == "__main__":

    print('Sumar 1 y 2:')
    sumar(1, 2)
    print()
    print('Sumar 3 y 4:')
    sumar(3, 4)
    print()
    print('Restar 5 de 6:')
    restar(6, 5)
    print()
    print('Restar 7 de 8:')
    restar(8, 7)
